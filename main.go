package main

import (
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"path"
)

func handleUpload(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	fmt.Println("[POST] File Upload")

	err := r.ParseMultipartForm(100 << 20)
	if err != nil {
		fmt.Println("\tfailed to parse form content")
		fmt.Printf("\t%s\n", err)
		http.Error(w, "Failed to parse form content", http.StatusBadRequest)
		return
	}

	file, handler, err := r.FormFile("img")
	if err != nil {
		fmt.Println("\tfailed to retrieve image from form content")
		fmt.Printf("\t%s\n", err)
		http.Error(w, "Failed to retrieve image from form content", http.StatusBadRequest)
		return
	}

	defer file.Close()

	fmt.Printf("\tfile name: %+v\n", handler.Filename)
	fmt.Printf("\tfile size: %+v\n", handler.Size)
	fmt.Printf("\tmime type: %+v\n", handler.Header)

	savedFilename := fmt.Sprintf("./assets/%d-%s", rand.Intn(1000), handler.Filename)
	savedFile, err := os.Create(savedFilename)
	if err != nil {
		fmt.Printf("\tfailed to create save file %s\n", savedFilename)
		http.Error(w, "Failed to create save file", http.StatusInternalServerError)
		return
	}
	defer savedFile.Close()

	bs, err := io.ReadAll(file)
	if err != nil {
		fmt.Println("\tfailed to read entire file content")
		fmt.Printf("\t%s\n", err)
		http.Error(w, "Failed to read entire file content", http.StatusBadRequest)
		return
	}

	savedFile.Write(bs)

	fmt.Printf("\tfile uploaded (%s)\n", savedFilename)
	w.WriteHeader(http.StatusOK)
}

func handleGet(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	filepath := r.URL.Path

	if filepath == "/" {
		filepath = "index.html"
	}

	p := path.Join("./assets", filepath)

	fmt.Printf("[GET] PATH:%s\n", p)

	file, err := os.Open(p)
	if err != nil {
		http.Error(w, "File not found", http.StatusNotFound)
		return
	}
	defer file.Close()

	fileStat, err := file.Stat()
	if err != nil {
		http.Error(w, "File not found", http.StatusNotFound)
		return
	}

	if fileStat.IsDir() {
		http.Error(w, "File is a directory", http.StatusNotFound)
		return
	}

	http.ServeContent(w, r, p, fileStat.ModTime(), file)
}

func main() {
	http.HandleFunc("/upload", handleUpload)
	http.HandleFunc("/", handleGet)

	fmt.Println("server starting at http://localhost:8080")
	http.ListenAndServe("localhost:8080", nil)
}